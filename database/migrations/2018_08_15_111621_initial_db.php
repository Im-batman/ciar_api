<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('cards', function(Blueprint $table){
		    $table->increments('cardid');
		    $table->integer('cid');
		    $table->string('authCode');
		    $table->string('cardNumber');
		    $table->string('cardType');
		    $table->timestamps();
	    });


	    Schema::create('likes', function(Blueprint $table){
	    	$table->increments('lid');
	    	$table->integer('cid');
	    	$table->integer('pid');
	    	$table->timestamps();
	    });
	    Schema::create('mood', function(Blueprint $table){
		    $table->increments('moid');
		    $table->integer('cid');
		    $table->string('name');
		    $table->string('description')->nullabe();
		    $table->timestamps();
	    });


	    Schema::create('mood_posts',function (Blueprint $table){
		    $table->increments('mpid');
		    $table->integer('moid');
		    $table->integer('pid');
		    $table->timestamps();
	    });

	    Schema::create('messages', function(Blueprint $table) {
		    $table->increments( 'mid' );
		    $table->integer( 'sender' );
		    $table->integer( 'receiver' );
		    $table->string( 'content', 5000 );
		    $table->boolean( 'fromSender' );
		    $table->boolean( 'isRead' )->default( 0 );
		    $table->timestamps();
	    });

	    Schema::create('payments',function (Blueprint $table){
		    $table->increments('pid');
		    $table->integer('oid');
		    $table->integer('cardid');
		    $table->integer('cid');
		    $table->string('amount');
		    $table->integer('charge');
		    $table->enum('status',['initiated','available','withdrawn','failed'])->default('initiated');
		    $table->string('reference',191)->unique();
		    $table->string('flw_reference',191)->unique()->nullable();
		    $table->timestamps();
	    });


	    Schema::create('orders', function (Blueprint $table) {
		    $table->increments('oid');
		    $table->integer('pid');
		    $table->integer('cid');
		    $table->string('quantity')->default(1);
		    $table->string('address');
		    $table->string('phone');
		    $table->string('notes',5000)->nullable();
		    $table->string('total');
		    $table->enum('paymentStatus',['unpaid','paid'])->default('unpaid');
		    $table->timestamps();
	    });


	    Schema::create('comments', function (Blueprint $table) {
		    $table->increments('comid');
		    $table->integer('pid');
		    $table->integer('cid');
		    $table->string('comment');
		    $table->integer('isReplyTo')->nullable(); //contains comid of the comment being replied to
		    $table->timestamps();
	    });

	    Schema::create('reviews', function(Blueprint $table){
	        $table->increments('revid');
	        $table->integer('cid');
	        $table->integer('pid');
	        $table->string('image')->nullable();
	        $table->string('review',5000);
	        $table->timestamps();
	    });

	    Schema::create('reported', function(Blueprint $table){
	        $table->increments('repid');
	        $table->integer('cid');
	        $table->integer('pid');
	        $table->timestamps();
	    });


	    Schema::create('events', function(Blueprint $table){
	        $table->increments('eid');
	        $table->string('name');
	        $table->string('description',5000);
	        $table->string('location');
	        $table->timestamps();
	    });

	    Schema::create('customers', function (Blueprint $table) {
		    $table->increments('cid');
		    $table->string('name');
		    $table->string('businessName');
		    $table->string('brandName');
		    $table->string('email',191)->unique();
		    $table->string('bio')->nullable();
		    $table->string('phone');
		    $table->string('username',191)->unique();
		    $table->string('password');
		    $table->string('facebook');
		    $table->string('instagram');
		    $table->string('methodOfID');
		    $table->string('twitter');
		    $table->string('website')->nullable();
		    $table->boolean('isApproved')->default(0);
		    $table->boolean('isSeller')->default(0);
		    $table->mediumText('sellerProfile')->nullable();
		    $table->string('others',5000)->nullable();
		    $table->rememberToken();
		    $table->timestamps();
	    });


	    Schema::create('posts', function (Blueprint $table) {
		    $table->increments('pid');
		    $table->integer('cid');
		    $table->string('caption')->nullable();
		    $table->string('price')->nullable();
		    $table->string('description')->nullable();
		    $table->string('colors')->nullable();
		    $table->string('sizes')->nullable();
		    $table->boolean('isProduct')->default(0);
		    $table->string('others',5000)->nullable();

		    $table->rememberToken();
		    $table->timestamps();
	    });

	    Schema::create('post_files', function (Blueprint $table) {
		    $table->increments('pfid');
		    $table->integer('pid');
		    $table->string('file');
		    $table->string('others',5000)->nullable();
		    $table->timestamps();
	    });



	    Schema::create('users', function (Blueprint $table) {
		    $table->increments('uid');
		    $table->string('name');
		    $table->string('email',191)->unique();
		    $table->string('password');
		    $table->rememberToken();
		    $table->timestamps();
	    });


	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email',191)->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });


	    Schema::create('settings', function (Blueprint $table) {
		    $table->increments('sid');
		    $table->integer('uid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();
	    });



	    Schema::create('customer_following', function(Blueprint $table){
		    $table->increments('cffid');
		    $table->integer('cid');
		    $table->integer('following');
		    $table->timestamps();
	    });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('cards');
	    Schema::dropIfExists('likes');
	    Schema::dropIfExists('mood');
	    Schema::dropIfExists('mood_posts');
	    Schema::dropIfExists('payments');
	    Schema::dropIfExists('orders');
	    Schema::dropIfExists('comments');
	    Schema::dropIfExists('reviews');
	    Schema::dropIfExists('reported');
	    Schema::dropIfExists('events');
	    Schema::dropIfExists('customers');
	    Schema::dropIfExists('posts');
	    Schema::dropIfExists('post_files');
	    Schema::dropIfExists('users');
	    Schema::dropIfExists('password_resets');
	    Schema::dropIfExists('settings');
    }
}
