<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class comment extends Model
{
    protected  $primaryKey = 'comid';
    protected  $table = 'comments';
    protected  $guarded = [];


	public function Post() {
		return $this->belongsTo(post::class,'pid','pid');
    }

}
