<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected  $table = 'posts';
    protected  $primaryKey = 'pid';
	protected  $guarded = [];

	public function Files() {
		return $this->hasMany(postFiles::class,'pid','pid');
	}

	public function Customer() {
		return $this->belongsTo(customer::class,'cid','cid');
	}

	public function Likes() {
		return $this->hasMany(like::class,'pid','pid');
	}

	public function Comments() {
		return $this->hasMany(comment::class,'pid','pid');
	}


	public function Orders(  ) {

		return $this->hasMany(order::class,'pid','pid');
	}

}
