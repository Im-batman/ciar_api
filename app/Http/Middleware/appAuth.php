<?php

namespace App\Http\Middleware;

use Closure;

class appAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $appAuthKey = setting::where('name', 'appAuthKey')->first()->value;

	    if($request->header('Auth-Key') == $appAuthKey){

		    return $next($request);
	    } else{
		    return response(array("message" => "Unauthorized"),400);
	    }

    }
}
