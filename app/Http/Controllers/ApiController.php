<?php

namespace App\Http\Controllers;

use App\card;
use App\comment;
use App\customer;
use App\following;
use App\like;
use App\message;
use App\mood;
use App\moodPosts;
use App\order;
use App\post;
use App\postFiles;
use App\customerFollowing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ApiController extends Controller
{

	public function signUp(Request $request ) {

		//todo validate api
		$name = $request->input('name');
		$brandName = $request->input('brandName');
		$businessName = $request->input('businessName');
		$methodOfID = $request->input('methodOfID');
		$facebook = $request->input('facebook');
		$twitter = $request->input('twitter');
		$instagram = $request->input('instagram');
		$website = $request->input('website');
		$username = $request->input('username');
		$phone = $request->input('phone');
		$email = $request->input('email');


		$customer = customer::where("username",$username)->count();
		if($customer > 0)  return response(array("message" => "Username already exists"),406);

		$customer = customer::where("email",$email)->count();
		if($customer > 0)  return response(array("message" => "Email already exists"),406);

		$customer = customer::where("phone",$phone)->count();
		if($customer > 0)  return response(array("message" => "Phone number already exists"),406);


		if(empty($name)) return response(array("message" => "Name is required"),400);
		if(empty($brandName)) return response(array("message" => "Brand name is required"),400);
		if(empty($businessName)) return response(array("message" => "Business name is required"),400);
		if(empty($facebook)) return response(array("message" => "Facebook handle is required"),400);
		if(empty($instagram)) return response(array("message" => "Instagram handle is required"),400);
		if(empty($twitter)) return response(array("message" => "Twitter handle is required"),400);
		if(empty($username)) return response(array("message" => "Username is required"),400);
		if(empty($phone)) return response(array("message" => "Phone is required"),400);
		if(empty($email)) return response(array("message" => "Email is required"),400);

		$customers = new customer();
		$customers->name = $name;
		$customers->brandName = $brandName;
		$customers->businessName = $businessName;
		$customers->methodOfID = $methodOfID;
		$customers->facebook = $facebook;
		$customers->instagram = $instagram;
		$customers->twitter = $twitter;
		$customers->website = $website;
		$customers->username = $request->input('username');
		$customers->phone = $request->input('phone');
		$customers->email = $request->input('email');
		$customers->others = $request->input('others');
		$customers->password = bcrypt($request->input('password'));

		$customers->save();

		return response(array(['message'=>'Successful','data'=>$customers]),200);


	}

	public function signIn(Request $request ) {


		$email = $request->input('email');
		$password = $request->input('password');

		if(empty($email)) return response(array("message" => "Email is required"),400);
		if(empty($password)) return response(array("message" => "Password is required"),400);

		//check for existing email
		$customers =  customer::where('email',$email)->count();
		if($customers <= 0 ) return response(array("message" => "Email is not registered with us"),400);



		$customer = customer::where('email', $email)->first();

		if (count($customer)>0){
			if (password_verify($password,$customer->password)){

				try{$customer->Cards;}catch (\Exception $exception){}

				return response(array(['message'=>'Successful','data'=> $customer]),200);
			}else{
				return response(array(["message"=>'Sorry, account not found']),401);
			}
		}



	}

	public function checkUsername( Request $request) {
		$username = $request->input('username');

		if(!isset($username)) return response(array("message" => "Username is required"),406);

		$username = customer::where("username",$request->input('username'))->count();
		if($username > 0) return response(array('message' => "Unavailable"),406) ;
		else return array("message" => "Available");
	}

	public function changeUsername( Request $request ) {
		$cid = $request->input('cid');
		$username = $request->input('username');

		if(!isset($cid)) return response(array("message" => "CID is required"),406);
		if(!isset($username)) return response(array("message" => "Username is required"),406);


		$user = customer::find($cid);
		$user->username = $username;
		$user->save();

		return response(array("message" => "Successful", "data" => $user));
	}

	public function changeProfilePhoto( Request $request ) {
		$image = $request->file('image');
		$cid = $request->input('cid');

		if(!isset($image)) return response(array("message" => "Image is required."),400);
		if(!isset($uid)) return response(array("message" => "CID is required."),400);

		$inputFileName = Carbon::now()->timestamp . Str::random(6) . $image->getClientOriginalName();
		$image->move("uploads",$inputFileName);

		$user = customer::find($cid);
		$user->image = url('uploads/' . $inputFileName);
		$user->save();

		return response(array("message" => "Successful"));


	}

	public function followUser( Request $request ) {
		$cid = $request->input('cid');
		$following = $request->input('following');

		if(empty($cid)) return response(array("message" => "CID is required"),406);
		if(empty($following)) return response(array("message" => "Following is required"),406);

		if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);
		if(customer::where("cid",$following)->count() <= 0)  return response(array("message" => "Following User does not exist"),406);


		if(customerFollowing::where('cid',$cid)->where('following',$following)->count() > 0) return response(array("message" => "Already following."),400);

		$userFollowing = new customerFollowing();
		$userFollowing->cid = $cid;
		$userFollowing->following = $following;
		$userFollowing->save();

		return array("message" => "Successful","data" => $userFollowing);
	}

	public function unfollowUser(Request $request) {
		$cid = $request->input('cid');
		$following = $request->input('following');

		if(empty($cid)) return response(array("message" => "CID is required"),406);
		if(empty($following)) return response(array("message" => "Following is required"),406);


		if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);
		if(customer::where("cid",$following)->count() <= 0)  return response(array("message" => "Following User does not exist"),406);


		if(customerFollowing::where('cid',$cid)->where('following',$following)->count() <= 0) return response(array("message" => "Not following."),400);

		customerFollowing::where('cid',$cid)->where('following',$following)->first()->delete();

		return array("message" => "Successful");
	}

	public function changePassword( Request $request ) {

		try{
			$cid = $request->input('cid');


			if(!isset($cid)) return response(array("message" => "CID is required"),400);
			if(customer::where('cid',$cid)->count() <= 0 ) return response(array("message" => "User does not exist"),400);

			$password = $request->input('password');
			$newPassword = $request->input('newPassword');
			if(!isset($password)) return response(array("message" => "Password is required"),400);
			if(!isset($newPassword)) return response(array("message" => "New Password is required"),400);


			$customer = customer::find($cid);

			if ( password_verify( $password, $customer->password ) ) {

				$customer->password = bcrypt( $newPassword );
				$customer->save();

				return array("message" => "Successful","data" => $customer);

			} else {
				return response(array("message" => "Current password is wrong"),400);
			}


		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
			return response($exception,500);
		}
	}

	public function makePost(Request $request) {

		try{
			$cid = $request->input('cid');

			if(empty($cid)) return response(array("message" => "CID is required"),400);
			if(empty($request->file('files'))) return response(array("message" => "Post must contain a photo"),400);


			$post = new post();
			$post->cid = $cid;
			$post->caption = $request->input('caption');
			$post->description = $request->input('description');
			$post->price = $request->input('price');
			$post->colors = $request->input('colors');
			$post->sizes = $request->input('sizes');
			$post->isProduct = $request->input('isProduct');
			$post->others = $request->input('others');
			$post->save();

			if (!empty($request->file('files'))){
				foreach ($request->file('files') as $file){


					$filename = $cid . Carbon::now()->timestamp . $file->getClientOriginalName();
					$file->move('uploads',$filename);
					$fileUrl = url('uploads/' . $filename);

					$files = new postFiles();
					$files->pid = $post->pid;
					$files->file = $fileUrl;
					$files->save();

				}
			}

			try{$post->Files; $post->Customer;}catch (\Exception $exception){}
			return response(array('message'=>'Successful', "data" => $post),200);

		}catch (\Exception $exception){
			return response($exception,500);
//			Bugsnag::notifyException($exception);
//			return response("Something went wrong",500);
		}



	}



	public function saveBase64Image($data) {
		$now =  Carbon::now()->timestamp ;
		if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
			$data = substr($data, strpos($data, ',') + 1);
			$type = strtolower($type[1]); // jpg, png, gif

			if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
				throw new \Exception('invalid image type');
			}

			$data = base64_decode($data);

			if ($data === false) {
				throw new \Exception('base64_decode failed');
			}
		} else {
			throw new \Exception('did not match data URI with image data');
		}

		file_put_contents($now . "img.{$type}", $data);
		rename($now . "img.{$type}","uploads/" . $now. "img.{$type}" );

		return "uploads/" .$now. "img.{$type}";
	}

	public function deletePost(Request $request) {

		$cid = $request->input('cid');
		$pid = $request->input('pid');

		if(empty($cid)) return response(array("message" => "CID is required"),400);
		if(empty($pid)) return response(array("message" => "PID is required"),400);
		if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),400);
		if(post::where('pid',$pid)->count() <= 0 ) return response(array("message" => "Post does not exist"),400);

		$post = post::find($pid);

		if($post->cid == $cid) {

			try{
				foreach($post->Files as $file){
					$urlArray = explode("/",$file->file);
					$url = $urlArray[count($urlArray) - 2] . "/" . $urlArray[count($urlArray) - 1];
					unlink($url);
					postFiles::destroy($file->pfid);
				}
			}catch (\Exception $exception){Bugsnag::notifyException($exception);}

			$post->delete();


			return response(array("message" => "Successful"));
		}

		else return response(array("message" => "Unauthorized."),406);

	}

	public function updateProfile(Request $request) {

		try{

			$fname = $request->input('fname');
			$sname = $request->input('sname');
			$phone = $request->input('phone');
			$email = $request->input('email');
			$cid = $request->input('cid');

			if(!isset($cid)) return response(array("message" => "CID is required"),400);
			if(customer::where('cid',$cid)->count() <= 0 ) return response(array("message" => "User does not exist"),400);


			$customer = new customer();

			$customer->fname = $request->input('fname');
			$customer->sname = $request->input('sname');
			$customer->phone = $request->input('phone');
			$customer->email = $request->input('email');

			if(isset($email)){
				if(customer::where('email',$email)->count() <= 0 ) {
					$customer->email = $request->input('email');
				}

			}


			$customer->save();

			try{
				$customer->Cards;
			}catch (\Exception $exception){}
			return array("message" => "Success","data" => $customer);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response(array("message" => "An error occurred"),500);
		}
	}

	public function timeline(Request $request) {
		$cid = $request->input('cid');

		if(!isset($cid)) return response(array("message" => "CID is required"),400);
		if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);

		$customer = customer::find($cid);

		$following = $customer->Following;

		$followingArray = array($customer->cid);
		foreach($following as $item){
			array_push($followingArray,$item->cid);
		}
		$posts = post::whereIn('cid',$followingArray)->get();

		foreach($posts as $post){
			try{$post->Files;}catch (\Exception $exception){}
			try{$post->Costomer;}catch (\Exception $exception){}
			try{$post->Comments;}catch (\Exception $exception){}
			try{$post->Likes;}catch (\Exception $exception){}
			$post['noOfLikes'] = count($post->Likes);
			$post['noOfComments'] = count($post->Like);
		}

		return array("message" => "Successful","data" => $posts);

	}

	public function addCard( Request $request ) {
		$cid = $request->input('cid');
		$authCode = $request->input('authCode');
		$cardNumber = $request->input('cardNumber');


		if(!isset($cid)) return response(array("message" => "CID is required"),400);
		if(customer::where('cid',$cid)->count() <= 0)
			return response(array("message" => "Customer does not exist"),400);
		if(!isset($authCode)) return response(array("message" => "Auth code is required"),400);
		if(!isset($cardNumber)) return response(array("message" => "Card number is required"),400);
		if(card::where('cid',$cid)->where('cardNumber', $cardNumber)->count() > 0) return response(array("message" => "Card already exists"),400);

		$card = new card();
		$card->cid = $cid;
		$card->authCode = $authCode;
		$card->cardNumber = $cardNumber;
		$card->cardType = $request->input('cardType');
		$card->save();

		$customer = customer::find($cid);
		try{$customer->Cards;}catch (\Exception $exception){}

		return array("message" => "Success","data" => $customer );
	}

	public function deleteCard( Request $request ) {
		$cardid = $request->input('cardid');

		if(!isset($cardid)) return response(array("message" => "CARDID is required"),400);
		if(card::where('cardid',$cardid)->count() <= 0)
			return response(array("message" => "Card does not exist"),400);

		$card = card::find($cardid);
		$card->delete();

		$customer = $card->Customer;
		try{
			$customer->Cards;
		}catch (\Exception $exception){}

		return array("message" => "Card deleted", "data" => $customer);

	}

	public function cards( Request $request ) {
		$cid = $request->input('cid');
		if(!isset($cid)) return response(array("message" => "CID is required"),400);

		$customer = customer::find($cid);

		try{ $customer->Cards; }catch (\Exception $exception){}

		return array("message" => "Success", "data" => $customer);
	}

	public function makeComment(Request $request ) {

		try{
			$cid = $request->input('cid');
			$pid = $request->input('pid');
			$comm = $request->input('comment');
			$isReplyTo = $request->input('reply');


			if(empty($comm)) return response(array("message" => "Comment is required"),400);
			if(empty($pid)) return response(array("message" => "PID is required"),400);
			if(empty($cid)) return response(array("message" => "CID is required"),400);

			if(post::where('pid',$pid)->count() <= 0) return response(array("message" => "Post does not exist"),406);
			if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);
			if(isset($isReplyTo) && comment::where("comid",$isReplyTo)->count() <= 0)  return response(array("message" => "Comment to be replied to does not exist"),406);

			$comment = new comment();
			$comment->pid = $pid;
			$comment->cid = $cid;
			$comment->comment = $comm;
			$comment->isReplyTo = $isReplyTo;
			$comment->save();

			return response(array(['message'=> 'Successful',"data" => $comment]),200);

		}catch (\Exception $exception){

			Bugsnag::notifyException($exception);
			return response(array(['message'=> 'Sorry something went wrong']),400);
		}
	}

	public function deleteComment(Request $request) {

		try{
			$comid = $request->input('comid');
			$cid = $request->input('cid');

			if(!isset($cid)) return response(array("message" => "CID is required"),400);
			if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);

			if(empty($comid)) return response(array("message" => "COMID is required"),400);
			if(comment::where("comid",$comid)->count() <= 0)  return response(array("message" => "Comment does not exist"),406);


			$comment = comment::find($comid);
			if($comment->cid != $cid) return response(array("message" => "Unauthorized"),406);
			comment::destroy($comid);
			return response(array('message' => 'Successful'),200);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response(array("Something went wrong",$exception),400);
		}
	}

	public function like(Request $request) {
		$cid = $request->input('cid');
		$pid = $request->input('pid');


		try{

			if(!isset($cid)) return response(array("message" => "CID is required"),400);
			if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);
			if(empty($pid)) return response(array("message" => "PID is required"),400);
			if(post::where('pid',$pid)->count() <= 0 ) return response(array("message" => "Post does not exist"),400);

			if(like::where('cid',$cid)->where('pid',$pid)->count() > 0) return response(array("message" => "Already Liked"),400);

			$like = new like();
			$like->cid = $cid;
			$like->pid = $pid;
			$like->save();

			return response(array(["message" => "Successful", "data" => $like]),200);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response('Something went wrong',500);

		}

	}

	public function unLike(Request $request) {
		$cid = $request->input('cid');
		$pid = $request->input('pid');
		try{

			if(!isset($cid)) return response(array("message" => "CID is required"),400);
			if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);
			if(empty($pid)) return response(array("message" => "PID is required"),400);
			if(post::where('pid',$pid)->count() <= 0 ) return response(array("message" => "Post does not exist"),400);

			if(like::where('pid',$pid)->where('cid',$cid)->count() <= 0) return response(array("message" => "Not liked by User"),400);

			like::where('cid',$cid)->where('pid',$pid)->delete();

			return response(array("message" => "Successful"),200);

		}catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);
		}
	}

	public function likedPosts(Request $request) {

		$cid = $request->input('cid');

		if(!isset($cid)) return response(array("message" => "CID is required"),400);
		if(customer::where("cid",$cid)->count() <= 0)  return response(array("message" => "User does not exist"),406);


	}




	public function postsILike(Request $request,$cid) {
		$likes = like::where('cid',$cid)->get();
		return $likes;
	}
	public function myLikedPosts(  Request $request) {
		$cid = $request->input('cid');
		if (!isset($cid)) return response(['message'=>'CID is required'],400);

		try{
			$pids = [ ];
			$myLikedPosts = [];
			$myPosts = post::where('cid',$cid)->get();
			foreach ($myPosts as $post){
				array_push($pids,$post->pid);
			}
			$likes = like::with('posts')->get();
			foreach ($likes as $like){
				if (in_array($like->pid,$pids)){
					array_push($myLikedPosts,$like);
				}
			}

			return response(array(["message" => "Success", "data" => $myLikedPosts]),200);

		}catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);

		}
	}
	public function customerUpdatedDetails( Request $request ) {
		$cid = $request->input('cid');
		if(!isset($cid)) return response(array("message" => "CID is required"),400);
		if(customer::where('cid',$cid)->count() <= 0)
			return response(array("message" => "Customer does not exist"),400);

		$customer = customer::find($cid);
		try{$customer->Cards;}catch (\Exception $exception){}
		return array("message" => "Successful","data" => $customer );

	}
	public function postSearch( Request $request ) {

		$term = $request->input('term');
		if (!isset($term)) return response(['message'=>'No Search Term'],400);
		$posts = post::where('caption','like',"%#$term%")->orwhere('description','like',"%$term%")->get();
		foreach($posts as $item){
			$item->Images;
			$item->User;
		}
		return $posts;

	}




	public function findBankCode( $bank ) {

		$data = '{
"status": "success",
"message": "Banks",
"data": [
{
"name": "ACCESS BANK NIGERIA",
"code": "044",
"country": "NG"
},
{
"name": "ACCESS MOBILE",
"code": "323",
"country": "NG"
},
{
"name": "AFRIBANK NIGERIA PLC",
"code": "014",
"country": "NG"
},
{
"name": "Aso Savings and Loans",
"code": "401",
"country": "NG"
},
{
"name": "DIAMOND BANK PLC",
"code": "063",
"country": "NG"
},
{
"name": "Ecobank Mobile",
"code": "307",
"country": "NG"
},
{
"name": "ECOBANK NIGERIA PLC",
"code": "050",
"country": "NG"
},
{
"name": "ENTERPRISE BANK LIMITED",
"code": "084",
"country": "NG"
},
{
"name": "FBN MOBILE",
"code": "309",
"country": "NG"
},
{
"name": "FIDELITY BANK PLC",
"code": "070",
"country": "NG"
},
{
"name": "FIRST BANK PLC",
"code": "011",
"country": "NG"
},
{
"name": "FIRST CITY MONUMENT BANK PLC",
"code": "214",
"country": "NG"
},
{
"name": "GTBank Mobile Money",
"code": "315",
"country": "NG"
},
{
"name": "GTBANK PLC",
"code": "058",
"country": "NG"
},
{
"name": "HERITAGE BANK",
"code": "030",
"country": "NG"
},
{
"name": "KEYSTONE BANK PLC",
"code": "082",
"country": "NG"
},
{
"name": "Parkway",
"code": "311",
"country": "NG"
},
{
"name": "PAYCOM",
"code": "305",
"country": "NG"
},
{
"name": "SKYE BANK PLC",
"code": "076",
"country": "NG"
},
{
"name": "STANBIC IBTC BANK PLC",
"code": "221",
"country": "NG"
},
{
"name": "Stanbic Mobile",
"code": "304",
"country": "NG"
},
{
"name": "STANDARD CHARTERED BANK NIGERIA LIMITED",
"code": "068",
"country": "NG"
},
{
"name": "STERLING BANK PLC",
"code": "232",
"country": "NG"
},
{
"name": "UNION BANK OF NIGERIA PLC",
"code": "032",
"country": "NG"
},
{
"name": "UNITED BANK FOR AFRICA PLC",
"code": "033",
"country": "NG"
},
{
"name": "UNITY BANK PLC",
"code": "215",
"country": "NG"
},
{
"name": "WEMA BANK PLC",
"code": "035",
"country": "NG"
},
{
"name": "ZENITH BANK PLC",
"code": "057",
"country": "NG"
},
{
"name": "ZENITH Mobile",
"code": "322",
"country": "NG"
},
{
"name": "Coronation Merchant Bank",
"code": "559",
"country": "NG"
},
{
"name": "FSDH Merchant Bank Limited",
"code": "601",
"country": "NG"
},
{
"name": "PARRALEX BANK",
"code": "526",
"country": "NG"
},
{
"name": "Providus Bank",
"code": "101",
"country": "NG"
}
]
}';

		$data = json_decode($data);


		foreach($data->data as $item){
			if($item->name == $bank){

				return $item->code;
			}
		}


	}
	public function posts(Request $request) {
		try{
			$posts = post::paginate(20);
			foreach($posts as $post){
				try{$post->Files;}catch (\Exception $exception){}
				try{$post->Customer;}catch (\Exception $exception){}
			}

			return response(array('message'=> 'Successful', 'data'=>$posts),200);
		}catch (\Exception $exception){
			return response(['message'=> 'Something went wrong'],500);
		}
	}
	public function myPosts(Request $request) {
		try{
			$cid = $request->input('cid');
			if (!isset($cid)) return response(['message'=>'CID is required'],400);
			$post = post::where('cid',$cid)->get();
			return response(array(['message'=> 'fetch successful', 'data'=> $post]),200);
		}catch (\Exception $exception){
			return response(array(['message'=> 'Something went wrong', $exception]),500);
		}
	}
	public function payment( Request $request) {

		DB::transaction(function (){

			global $request;

			$email = $request->input('email');
			$did = $request->input('did');
			$amount = $request->input('amount');
			$cardid = $request->input('cardid');
			$ip = $request->ip();

			if(!isset($email)) return response(array("message" => "Email is required"),400);
			if(!isset($did)) return response(array("message" => "DID is required"),400);
			if(!isset($amount)) return response(array("message" => "Amount is required"),400);
			if(donation::where('did',$did)->count() <= 0)
				return response(array("message" => "Donation does not exist"),400);


			if(!isset($cardid)) return response(array("message" => "CARDID is required"),400);
			if(card::where('cardid',$cardid)->count() <= 0)
				return response(array("message" => "Card does not exist"),406);

			$donation = donation::find($did);

			if($donation->status != "active")
				return response(array("message" => "Payments cannot be made to this donation at this time. Donation is disabled"),400);

			$charge = setting::where('name', 'charge')->count();

			if($charge <= 0){
				$setting = new setting();
				$setting->uid = 1;
				$setting->name = 'charge';
				$setting->value = 50;
				$setting->save();
				$charge = setting::where( 'name', 'charge' )->orderBy('created_at','desc')->get()->first()->value;
			}



			if(customer::where('email',$email)->count() <= 0){
				$password = strtoupper( Str::random(6) );
				$customer = new customer();
				$customer->fname = "Anonymous";
				$customer->sname = "Anonymous";
				$customer->email = $request->input('email');
				$customer->password = bcrypt($password);
				$customer->save();

				try{
					Mail::to( $email )->send( new autoSignUpMail( $email, $password ) );
				}catch (\Exception $exception){Bugsnag::notifyException($exception);}

			} else{
				$customer = customer::where('email', $email)->first();
			}

			//get current charge
//			$charge = setting::where( 'name', 'charge' )->orderBy('created_at','desc')->get()->first()->value;

			$payment = new payment();
			$payment->cid = $customer->cid;
			$payment->did = $request->input('did');
			$payment->amount = $request->input('amount') + $charge;
			$payment->reference = strtoupper("GN" . Str::random(10));
			$payment->status = 'initiated';
			$payment->charge = $charge;
			$payment->save();


			return $this->chargeSavedCard($payment->pid,$cardid,$ip);

		},3);

	}
	public function chargeSavedCard($pid, $cardid,$ip) {

		if(!isset($pid)) return response(array("message" => "PID is required"),400);
		if(payment::where('pid',$pid)->count() <= 0)
			return response(array("message" => "Payment transaction does not exist"),400);

		$payment = payment::find($pid);

		if($payment->status == "available" || $payment->status == "withdrawn" ) return response(array("message" => "Payment already credited", "data" => $payment),400);


		try{
			$card = card::find($cardid);

			$customer = $card->Customer;
			$cid = $customer->cid;

			if(!isset($cid)) return response(array("message" => "CID is required"),400);


			$customer = customer::find($cid); // get the customer

			$tokenizeUrl = $this->tokenizeUrl;

			$token = $card->authCode;
			$secret_key = env('SECRET_KEY');
			$email = $customer->email;
			$txRef = strtoupper("GN" . Str::random(10));
			$amount = $payment->amount;
			$country = "NG";
			$currency = "NGN";


			$data = [

				'currency' => $currency,
				'country' => $country,
				'ip' => $ip,
				'txRef' => $txRef,
				'email'=> $email,
				'amount'=> $amount,
				'token' => $token,
				'SECKEY'=> $secret_key,

			];



			$tokenize = new client();

			try{

				$post = $tokenize->request('POST',$tokenizeUrl,[
					'form_params'=> $data
				]);

				$response = $post->getBody()->getContents();

				$data = json_decode($response);

				if($data->status == "success" ){

					if($data->data->chargeResponseCode == "00"){ // if this is true then we confirm the payment


						$payment = payment::find($pid);

						if($payment->status == "initiated") {
							$payment->flw_reference  = $data->data->flwRef;
							$payment->cardNumber = $card->cardNumber;
							$payment->cardType = $card->cardType;
							$payment->status = "available";
							$payment->save();

							try{
								Mail::to($payment->Customer->email)->send(new paymentMail($payment));
							}catch (\Exception $exception){
								Bugsnag::notifyException($exception);
							}


							if ( $payment->Donation->type == "institution" ) {
								$user                     = User::find( $payment->Donation->User->uid );
								$user->available_earnings += $payment->amount;
								$user->total_earnings     += $payment->amount;
								$user->save();
							}

							if ( $payment->Donation->type == "individual" ) {
								$customer                  = customer::find( $payment->Donation->Customer->cid );
								$customer->available_earnings += $payment->amount;
								$customer->total_earnings     += $payment->amount;
								$customer->save();
							}

						}

					}
				} else {
					$payment->status = 'failed';
					$payment->save();
					return array("message" => "Failed","data"=>$payment);
				}


				return array("message" => "Successful","data" => $payment);

			}catch (\Exception $exception){
				$payment->status = 'failed';
				$payment->cardNumber = $card->cardNumber;
				$payment->cardType = $card->cardType;
				$payment->save();

				Bugsnag::notifyException($exception);
				return response(array("message" => "Failed. ","data" => $exception->getMessage()),500);
			}




		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response(array("message" => "An error occurred"),500);
		}

	}


	public function makeOrder(Request $request, $cid) {
		$pids = $request->input('pid');
		if (!isset($pids)) return response(['message' => 'At least 1 pid is required'],400);
		try{
			foreach ($pids as $pid){
				$order = new order();
				$order->pid = $pid;
				$order->cid = $cid;
				$order->quantity = $request->input('quantity');
				$order->address = $request->input('address');
				$order->phone = $request->input('phone');
				$order->notes = $request->input('notes');
				$order->total = $request->input('total');
				$order->save();
				return response(array(['message'=> 'order made Successfully', $order]),200);
			}
		}catch (\Exception $e){
			return response(array(['message'=>'Sorry something went wrong', $e]),401);
		}
	}
	public function getCards($cid) {
		if (!isset($cid)) return response(['message'=> 'Cid is required']);

		try{
			$cards = card::where('cid',$cid)->get();
			return response(array(['message'=>'card fetch successfully', $cards]),200);

		}catch(\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);

		}
	}
	public function follow(Request $request ) {
		$cid = $request->input('cid');
		$followingId = $request->input('followingId');
		if(!isset($cid)) return response(['message' => 'CID is required'],400);
		if (!isset($followingId)) return response(['message' => 'Follow_ID is required'],400);

		try{
			$follow = new customerFollowing();
			$follow->cid = $cid;
			$follow->following = $followingId;
			$follow->save();

			return response(array(['message'=>'follow successful', $follow]),200);

		}catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);

		}
	}
	public function unfollow($cid,$toUnFollowId) {
		if(!isset($cid)) return response(['message' => 'CID is required'],400);
		if(!isset($toUnFollowId)) return response(['message'=> 'UnFollow id is required'],400);

		try{
			$unfollow = customerFollowing::where('cid',$cid)->Where('following',$toUnFollowId)->delete();
			return response(array(['message'=>'Unfollow successful', $unfollow]),200);

		}	catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);
		}
	}
	public function followers(Request $request) {
		try{
			$cid = $request->input('cid');
			if (!isset($cid)) return response(['message' => 'CID is required'],400);
//		$customer = customer::find($cid);
//		$followers = customerFollowing::with('customer')->where('following',$cid)->get();
			$followers = customerFollowing::where('following',$cid)->get();
			return $followers;

		}catch (\Exception $e){

		}





	}
	public function Following(  Request $request) {
		$cid = $request->input('cid');
		if (!isset($cid)) return response(['message'=> 'CID is required'],400 );
		$followers = customerFollowing::where('cid',$cid)->get();
		return $followers;
	}
	public function addMood(Request $request  ) {
		$cid = $request->input('cid');
		if (!isset($cid)) return response(['message' => 'CID is required'],400);
		try{
			$mood  = new mood();
			$mood->cid = $cid;
			$mood->name = $request->input('name');
			$mood->description = $request->input('description');
			$mood->save();
			return response(array(["message" => "Success", "data" => $mood]),200);
		}catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);

		}

	}

	public function deleteMood(Request $request,$moid  ) {
		if (!isset($moid)) return response(['message' => 'Mood id is required'],400);
//		$moid = $request->input('moid');
		$mode_item = moodPosts::where('moid',$moid)->count();
		if ($mode_item >0){
			return response(array(["message" => "Mood contains posts, Cant delete", "data" => $mode_item]),400);
		}else{
			try{
				$mood = mood::destroy($moid);
				return response(array(["message" => "Success", "data" => $mood]),200);
			}catch (\Exception $e){
				return response(array(['message'=>'something went wrong', $e]),400);
			}
		}

	}

	public function myMood(Request $request,$cid) {
		if (!isset($cid)) return response(['message' => ' CID is required'],400);

//		$cid = $request->input('cid');
		try{
//			$mood = mood::with('mood_posts')->where('cid',$cid)->get();
			$mood = mood::where('cid',$cid)->get();
			return response(array(["message" => "Success", "data" => $mood]),200);

		}catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);
		}
	}
	public function addMoodPosts(Request $request  ) {
		$moid = $request->input('moid');
		$pid = $request->input('pid');
		if (!isset($moid)) return response(['message' => 'Mood id is required'],400);
		if (!isset($pid)) return response(['message' => ' post id is required'],400);

		try {
			$post       = new moodPosts();
			$post->pid  = $pid;
			$post->moid = $moid;
			$post->save();
			return response( array( [ "message" => "Success", "data" => $post ] ), 200 );

		}catch (\Exception $e){
			return response(array(['message'=>'something went wrong', $e]),400);

		}


	}
	public function messageUsers( Request $request ) {

		$uid = $request->input('uid');
		if (!isset($uid)) return response(['message' => 'User_id is required'],400);


		$users = array();
		$messages = message::all()->where('receiver', $uid);
		foreach($messages as $item){
			array_push($users,$item->Sender);
		}

		$messages = message::all()->where('sender', $uid);
		foreach($messages as $item){
			array_push($users,$item->Receiver);
		}

		$users = collect($users)->unique()->flatten();

		foreach($users as $user){
			$lastMessage = message::where('sender',$user->uid)->orWhere('receiver',$user->uid)->get()->last();
			$unread = message::where('sender',$user->uid)->orWhere('receiver',$user->uid)->where('isRead',0)->count();
			$user['lastMessage'] = $lastMessage;
			$user['unread'] = $unread;
		}

		return $users;
	}
	public function sendMessage( Request $request ) {
		$sender = $request->input('sender');
		$receiver = $request->input('receiver');
		$content = $request->input('content');
		if (!isset($sender)) return response(['message' => 'sender is required'],400);
		if (!isset($receiver)) return response(['message' => 'receiver is required'],400);
		if (!isset($content)) return response(['message' => 'content is required'],400);


		try{
			$message = new message();
			$message->sender = $sender;
			$message->receiver = $receiver;
			$message->content = $content;
			$message->save();

			return array("message" => "Successful", "data" => $message);

		}catch (\Exception $exception){

			return response(array("message" => "error"),500);
		}

	}
	public function conversation(Request $request) {
		$sender = $request->input('sender');
		$receiver = $request->input('receiver');
		if (!isset($sender)) return response(['message' => 'sender is required'],400);
		if (!isset($receiver)) return response(['message' => 'receiver is required'],400);

		$sentMessages = message::where('receiver',$receiver)->where('sender',$sender)->get();
		$receivedMessages = message::where('receiver',$sender)->where('sender',$receiver)->get();


		$messages = collect($sentMessages)->merge($receivedMessages)->sortBy('created_at',SORT_ASC)->flatten();
//$messages = collect($sentMessages)->merge($receivedMessages)->flatten();

		foreach($messages as $message){
			if($message->sender == $sender)
				$message->fromSender = 1;
			else
				$message->fromSender = 0;
		}
		return $messages;
	}
	public function setMessageRead(Request $request) {
		$mid = $request->input('mid');
		if (!isset($mid)) return response(['message' => 'Mid is required'],400);



		if(message::where('mid', $mid)->count() <= 0) return response(array("message" => "Message does not exist"),400);

		$message = message::find($mid);
		if($message->isRead == 1) return response(array("message" => "Already read.","data" => $message),406);

		$message = message::find($mid);
		$message->isRead = 1;
		$message->save();
		return array("message" => "Successful","data" => $message);
	}


	public function deleteMessage( Request $request ) {
		$mid = $request->input('mid');
		if (!isset($mid)) return response(['message' => 'MID is required'],400);


		if(message::where('mid', $mid)->count() <= 0) return response(array("message" => "Message does not exist"),400);

		message::destroy($mid);
		return array("message" => "Message Deleted.", "mid" => $mid);
	}

	public function deleteConversation( Request $request ) {
		$sender = $request->input('sender');
		$receiver = $request->input('receiver');

		if (!isset($sender)) return response(['message' => 'sender is required'],400);
		if (!isset($receiver)) return response(['message' => 'receiver is required'],400);

		$messages = message::where('receiver',$receiver)->where('sender',$sender)->get();
		foreach($messages as $message){
			message::destroy($message->mid);
		}
		return array("message" => "Conversation Deleted.");

	}


	public function verifyTransaction(Request $request) {

		$url = 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/v2/verify';
		$txRef = $request->input('txRef');
		$Secret_key = env('secret_key');
		$data = [
			'txRef' => $txRef,
			'SECKEY' => $Secret_key,
		];
		$verify = new client();
		$post = $verify->request('POST', $url,[
			'form_params' => $data
		]);

		$response = $post->getBody()->getContents();

		return $response;

	}



	
	
	//orders and sales

	public function updatePost( Request $request ) {
		$pid =  $request->input('pid');
		$cid = $request->input('cid');
		if (!isset($pid)) return response(['message' => 'PID is required'],400);
		if (!isset($cid)) return response(['message' => 'CID is required'],400);
		try{

			$post = post::find($pid);
//			$post->caption = $request->input('caption');
//			$post->description = $request->input('description');
//			$post->price = $request->input('price');
//			$post->colors = $request->input('colors');
//			$post->sizes = $request->input('sizes');
//			$post->isProduct = $request->input('isProduct');
//			$post->others = $request->input('others');
			$post->update($request->all());
			$post->save();


			if (!empty($request->file('files'))){
				foreach ($request->file('files') as $file){


					$filename = $cid . Carbon::now()->timestamp . $file->getClientOriginalName();
					$file->move('uploads',$filename);
					$fileUrl = url('uploads/' . $filename);

					$files = new postFiles();
					$files->pid = $post->pid;
					$files->file = $fileUrl;
					$files->save();

				}
			}

			try{$post->Files; $post->Customer;}catch (\Exception $exception){}
			return response(array('message'=>'Update Successful', "data" => $post),200);

		}catch (\Exception $e){
			return $e;
		}

	}

	public function getOrdersSummary(Request $request,$cid) {

		try{

			if (!isset($cid)) return response(['message' => 'Cid is required'],400);

			$orders = order::all();
			$posts = post::where('cid',$cid)->get();
			$postIds = [];
			$myorderes = [];
			foreach ($posts as $post){
				array_push($postIds,$post->pid);
			}
			foreach ($orders as $order){

				if (in_array($order->pid,$postIds)){
					array_push($myorderes,$order);
				}

			}

			$salesAmount = [];
			$total_Sales = array_sum($salesAmount);

			foreach ($myorderes as $order){
				$order->Posts;
				$order->Customer;
				array_push($salesAmount,$order->total);
			}



			return response(['message' => 'Succeful', 'data' => $myorderes],200);

		}catch (\Exception $e){
			return response(['message' => 'failed','error' => $e],400);
		}


	}







	// payments


	//encrypt the key md5
	function getKey($seckey){
		$hashedkey = md5($seckey);
		$hashedkeylast12 = substr($hashedkey, -12);

		$seckeyadjusted = str_replace("FLWSECK-", "", $seckey);
		$seckeyadjustedfirst12 = substr($seckeyadjusted, 0, 12);

		$encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;
		return $encryptionkey;

	}
	//3des encryption
	function encrypt3Des($data, $key)
	{
		$encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
		return base64_encode($encData);
	}
	//pay with card
	public function pay( Request $request ) {

		$public_key = env('public_key');
		$secret_key = env('secret_key');
		$url = 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/charge';

		$PBFPubKey = $public_key;
		$cardno = $request->input('card_number');
		$cvv = $request->input('cvv');
		$pin = $request->input('pin');
		$expirymonth = $request->input('expirymonth');
		$expiryyear = $request->input('expiryyear');
		$passcode = $request->input('dob');
		$currency = "NGN";
		$amount = $request->input('amount');
		$email = $request->input('email');
		$ip = $request->ip();
		$txRef = "QP" . strtoupper(Str::random(16));
		$fname = $request->input('fname');
		$sname = $request->input('sname');
		$cardno = $string = str_replace(' ', '', $cardno);





// sending to local database
//		$transaction = new transaction();
//		$transaction->cardNumber = substr($cardno,strlen($cardno)-6);
//		$transaction->cvv = $cvv;
//		$transaction->expirymonth = $expirymonth;
//		$transaction->rxpiryyear = $expiryyear;
//		$transaction->currency = $currency;
//		$transaction->email = $email;
//		$transaction->txRef = $txRef;
//		$transaction->amount = $amount;
//		$transaction->name = $name;
//		$transaction->ip = $ip;
//		$transaction->status = "Initiated";
//		$transaction->save();



		if (isset($passcode)){
			$data = [
				'PBFPubKey' => $PBFPubKey,
				'cardno' => $cardno,
				'cvv' => $cvv,
				'firstname' => $fname,
				'lastname'=> $sname,
				'amount'=> $amount,
				'expirymonth' =>$expirymonth,
				'expiryyear' => $expiryyear,
				'email'=> $email,
				'passcode' => $passcode,
				'IP'=> $ip,
				'txRef' =>$txRef
			];

		}else{

			$data = [
				'PBFPubKey' => $PBFPubKey,
				'cardno' => $cardno,
				'cvv' => $cvv,
				'firstname' => $fname,
				'lastname'=> $sname,
				'amount'=> $amount,
				'expirymonth' =>$expirymonth,
				'expiryyear' => $expiryyear,
//			'suggested_auth' => 'pin',
				'email'=> $email,
				'IP'=> $ip,
				'txRef' =>$txRef
			];
		}




		$key = $this->getKey($secret_key); //encrypt secret key
		$data_req = json_encode($data); // encode data

		$encrypted_post_data =  $this->encrypt3Des($data_req,$key); //encrypt the data and they key together

		//post the data to flutter
		$Data_for_post = [
			'client' => $encrypted_post_data,
			'PBFPubKey' => $public_key,
			'alg' => '3DES-24'
		];

		$client  = new client();
		$request = $client->request('POST',$url,['form_params'=> $Data_for_post]);
		$response =  $request->getBody()->getContents();


//		return $response;
		$json_response = json_decode($response);

//		return $json_response;
		//pass the response for nigerian cards

		if ($json_response->status == 'success' and isset($json_response->data->suggested_auth)){

			if($json_response->data->suggested_auth == 'PIN'){

				//verify the card transaction
				$data = [
					'PBFPubKey' => $public_key,
					'cardno' => $cardno,
					'cvv' => $cvv,
					'amount'=> $amount,
					'expirymonth' =>$expirymonth,
					'expiryyear' => $expiryyear,
					'suggested_auth' => 'PIN',
					'pin' => $pin,
					'email'=> $email,
					'IP'=> $ip,
					'txRef' =>$txRef
				];

				//encode the data a second time with pin now
				$second_enc_data = json_encode($data);
				$second_encrypt = $this->encrypt3Des($second_enc_data,$key);
				$second_data_post = [
					'client' => $second_encrypt,
					'PBFPubKey'=> $public_key,
					'alg' => '3DES-24'

				];

				$post = new Client();
				$client = $post->request('POST',$url,[
					'form_params' => $second_data_post
				]);
				$new_response = $client->getBody()->getContents();

				$json_data = json_decode($new_response);
				$flwRef = $json_data->data->flwRef;

//				return $new_response;
//			 return $flwRef;
				if ($json_data->status == 'success'){
					return response(array('message'=> 'All went well!!!, transaction started. verify the card transaction now','data'=> $json_data),200);

				}else{
					return response(array('message' => 'card information is bad'));
				}

			}elseif ($json_response->data->suggested_auth == 'NOAUTH_INTERNATIONAL'){

//				return json_encode($json_response);

//				return "hello there";


				$data = [
					'PBFPubKey' => $public_key,
					'cardno' => $cardno,
					'cvv' => $cvv,
					'amount'=> $amount,
					'expirymonth' =>$expirymonth,
					'expiryyear' => $expiryyear,
//				    'suggested_auth' => 'pin',
//				    'pin' => '3242',
					'email'=> $email,
					'IP'=> $ip,
					'txRef' =>$txRef,
					'suggested_auth' => 'NOAUTH_INTERNATIONAL',
					'billing_zip' => 'zip',
					'billingcity' => 'city',
					'billingstate' => 'state',
					'billingaddress' => 'address',
					'billingcountry' => 'country',
					"redirect_url"=> "https://rave-webhook.herokuapp.com/receivepayment"

				];

				$second_enc_foreign_data = json_encode($data);  //encode data again
				$foreign_enc_foreign_data = $this->encrypt3Des($second_enc_foreign_data,$key); //encrypt data and key
				$foreign_data_post = [
					'client'=> $foreign_enc_foreign_data,
					'PBFPubKey'=> $public_key,
					'alg' => '3DES-24'
				];
				$post = new Client();
				$client = $post->request('POST',$url,[
					'form_params' => $foreign_data_post
				]);
				$foreign_response = $client->getBody()->getContents();

				return $foreign_response;

				$foreign_response = json_decode($foreign_response);

//
//				if ($foreign_response->status == 'success' and $foreign_response->data->suggested_auth == 'NOAUTH_INTERNATIONAL'){
//
//					$data = [
//						'PBFPubKey' => $public_key,
//						'cardno' => $cardno,
//						'cvv' => $cvv,
//						'amount'=> $amount,
//						'expirymonth' =>$expirymonth,
//						'expiryyear' => $expiryyear,
////				    'suggested_auth' => 'pin',
////				    'pin' => '3242',
//						'email'=> $email,
//						'IP'=> $ip,
//						'txRef' =>$txRef,
//					'suggested_auth' => 'NOAUTH_INTERNATIONAL',
//					'billing_zip' => 'zip',
//					'billingcity' => 'city',
//					'billingstate' => 'state',
//					'billingaddress' => 'address',
//					'billingcountry' => 'country',
//						"redirect_url"=> "https://rave-webhook.herokuapp.com/receivepayment"
//					];
//
//					$second_enc_foreign_data = json_encode($data);  //encode data again
//					$foreign_enc_foreign_data = $this->encrypt3Des($second_enc_foreign_data,$key); //encrypt data and key
//					$foreign_data_post = [
//						'client'=> $foreign_enc_foreign_data,
//						'PBFPubKey'=> $public_key,
//						'alg' => '3DES-24'
//					];
//					$post = new Client();
//					$client = $post->request('POST',$url,[
//						'form_params' => $foreign_data_post
//					]);
//					$foreign_response = $client->getBody()->getContents();
//
//					$foreign_response = json_decode($foreign_response);
//
//
//					return response(array('message'=> 'went well?. please verify the transaction','data'=> $foreign_response),200);
//
//				}else{
//
//					return 'something went wrong';
//				}






			}else{
				return response(array('message'=> 'The card information are incorrect'),400);
			}


			//for international cards
		}else if ($json_response->status == 'success' and isset($json_response->data->authModelUsed)){

//			return json_encode($json_response);
			if ($json_response->data->authModelUsed  == 'NOAUTH_INTERNATIONAL' or $json_response->data->authModelUsed == 'VBVSECURECODE'){

				//collect add the billingZip,billingStat etc to the request
				$data = [
					'PBFPubKey' => $public_key,
					'cardno' => $cardno,
					'cvv' => $cvv,
					'amount'=> $amount,
					'expirymonth' =>$expirymonth,
					'expiryyear' => $expiryyear,
					'email'=> $email,
					'IP'=> $ip,
					'txRef' =>$txRef,
//					'suggested_auth' => 'NOAUTH_INTERNATIONAL',
//					'billing_zip' => 'zip',
//					'billingcity' => 'city',
//					'billingstate' => 'state',
//					'billingaddress' => 'address',
//					'billingcountry' => 'country'
				];

				$second_enc_foreign_data = json_encode($data);  //encode data again
				$foreign_enc_foreign_data = $this->encrypt3Des($second_enc_foreign_data,$key); //encrypt data and key
				$foreign_data_post = [
					'client'=> $foreign_enc_foreign_data,
					'PBFPubKey'=> $public_key,
					'alg' => '3DES-24'
				];
				$post = new Client();
				$client = $post->request('POST',$url,[
					'form_params' => $foreign_data_post
				]);
				$foreign_response = $client->getBody()->getContents();

				$foreign_response = json_decode($foreign_response);


				if ($foreign_response->status == 'success'){
					return response(array('message'=> 'went well...., please verify the transaction','data'=> $foreign_response),200);

				}else{

					return 'something went wrong';
				}


			}else{
				return  response(array('message' => 'Verification mode unrecognized'),500);
			}



		}else{
			return response(array("message" => " card details are not complete"),400);
		}

	}
	//validate card payment
	public function validatePayment(Request $request) {
		$validateUrl = 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/validatecharge';
		$public_key = env('public_key');
		$secret_key = env('secret_key');

		$otp = $request->input('otp');
		$transaction_reference = $request->input('flwRef');

//		return $request->all();

		$data = [
			'PBFPubKey' => $public_key,
			'transaction_reference' => $transaction_reference,
			'otp' => $otp
		];

		$validate = new client();
		$post = $validate->request('POST',$validateUrl,[
			'form_params'=> $data
		]);


		$response = $post->getBody()->getContents();

		return $response;


	}
	//charge the card again after fist payment
	public function savedCardTokenPayment(Request $request  ) {

		$tokenizeUrl = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/tokenized/charge";

		$token = $request->input('embedToken');
		$secret_key = env('secret_key');
		$email = $request->input('email');
		$fname = $request->input('fname');
		$sname = $request->input('sname');
		$txRef = "QP" . strtoupper(Str::random(16));
		$amount =  $request->input('amount');
		$ip = $request->ip();
		$country = "NG";
		$currency = "NGN";


		$data = [

			'currency' => $currency,
			'country' => $country,
			'ip' => $ip,
			'txRef' => $txRef,
			'email'=> $email,
			'amount'=> $amount,
			'firstname'=> $fname,
			'lastname'=> $sname,
			'token' => $token,
			'SECKEY'=> $secret_key

		];

		$tokenize = new client();
		$post = $tokenize->request('POST',$tokenizeUrl,[
			'form_params'=> $data
		]);

		$response = $post->getBody()->getContents();
		return $response;

	}
	//verify saved card transaction
	public function VerifySavedCardPayment(Request $request  ) {

//		$url = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/xrequery";
		$url = "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify";
		$secret_key = env('secret_key');
		$data = [
			'txref' => $request->input('txref'),
			'SECKEY' => $secret_key,
			'inlcude_payment_entity' => 1
		];

		$post = new client();
		$verify = $post->request('POST',$url,[
			'form_params' => $data
		]);

		$response = $verify->getBody()->getContents();

		return $response;





	}
	public function postPaid(Request $request) {


		try{


			if($request->header('Verif-Hash') == "@Give1010"){
				Bugsnag::notifyException(new RuntimeException($request));
				$reference = $request->input('txRef');
				$entity = json_decode($request->input('entity'));
				$cardNumber = $entity->card_last4;

				if(strstr($reference,"givenow-payment") != false){ // check if its an add card transaction

					$this->verifyCardWasAdded($reference);

				} else { // transaction isn't a card addition transaction

					if($request->input('status') == "successful") // check if request was successful

						$payment =  customer::where('reference',$reference)->count();
					if($payment <= 0 ) return response(array("message" => "Payment transaction doesn't exist"),200);

					$payment = payment::where('reference',$reference)->first();
					$pid = $payment->pid;

					$payment = payment::find($pid);

					if($payment->status == "initiated" || $payment->status == "failed" ) {
						$payment->cardNumber = $cardNumber;
						$payment->status = "available";
						$payment->save();

						if ( $payment->Donation->type = "institution" ) {
							$user                     = User::find( $payment->Donation->User->uid );
							$user->available_earnings += $payment->amount;
							$user->total_earnings     += $payment->amount;
							$user->save();
						}

						if($payment->Donation->type = "individual"){
							$customer = customer::find($payment->Donation->Customer->cid);
							$customer->availableEarnings += $payment->amount;
							$customer->totalEarnings += $payment->amount;
							$customer->save();
						}
					} else {
						return response('Payment already credited',200);
					}


				}


			}


		}catch (\Exception $exception){
			Bugsnag::notifyException(new RuntimeException($exception));
			return response(array("message" => "Failed"),500);
		}


		return response("Success",200);
	}

//
//
//
//	//Transaction verification
//	public function verifyTransaction(Request $request  ) {
//
////		$url = 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/v2/verify';
//		$url = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify';
//		$txRef = $request->input('txRef');
//		$Secret_key = env('secret_key');
//		$data = [
//			'txRef' => $txRef,
//			'SECKEY' => $Secret_key,
//			'Content-Type'=>  'application/json'
//		];
//		$verify = new  client();
//		$post = $verify->request('POST', $url,[
//			'form_params' => $data
//		]);
//		$response = $post->getBody()->getContents();
//
//		return $response;
//
//
//	}
//


}
