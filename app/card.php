<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class card extends Model
{

	protected $table = 'cards';
	protected  $primaryKey = 'cardid';
	protected  $guarded = [ ];

	public function Customer() {
		return $this->belongsTo(customer::class,'cid','cid');
	}

	public function Payments() {
		return $this->hasMany(payment::class,'cardid','cardid');
	}

}
