<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{

	protected $primaryKey = 'cid';
	protected $table = 'customers';
	protected $guarded = ['password'];
	protected $hidden = ['password'];


	public function Cards() {
		return $this->hasMany(card::class,'cid','cid');
	}

	public function Payments() {
		return $this->hasMany(payment::class,'cid','cid');
	}

	public function Comments() {
		return $this->hasMany(comment::class,'cid','cid');
	}

	public function Followers() {
		return $this->belongsToMany(customer::class,'customer_following','following','cid');
	}

	public function Following() {
		return $this->belongsToMany(customer::class,'customer_following','cid','following');
	}

}
