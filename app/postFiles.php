<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postFiles extends Model
{
    protected $table =  'post_files';
    protected  $primaryKey = 'pfid';
	protected  $guarded = [];

}
