<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{

	protected  $primaryKey = 'orid';
	protected  $table = 'orders';
	protected  $guarded = [];


	public function Posts(  ) {
		return $this->hasMany(post::class,'pid','pid');
	}

	public function Customer(  ) {
		return $this->belongsTo( customer::class,'cid');
	}


}
