<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    protected $primaryKey = 'eid';
    protected $table = 'events';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
