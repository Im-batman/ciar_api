<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mood extends Model
{
    protected $primaryKey = 'moid';
    protected $guarded = [];

    protected $table = 'mood';

	public function MoodPost(  ) {
		return $this->hasMany(moodPosts::class,'moid');
    }
}
