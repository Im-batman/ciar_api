<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//users
Route::post('sign-up','ApiController@signUp');
Route::post('sign-in','ApiController@signIn');

Route::post('change-username','ApiController@changeUsername');
Route::post('check-username','ApiController@checkUsername');

Route::post('change-profile-photo','ApiController@changeProfilePhoto');

Route::post('follow-user','ApiController@followUser');
Route::post('unfollow-user','ApiController@unfollowUser');

Route::post('change-password','ApiController@changePassword');

Route::post('make-post','ApiController@makePost');
Route::post('delete-post','ApiController@deletePost');

Route::post('update-profile','ApiController@updateProfile');

Route::post('timeline','ApiController@timeline');

//cards
Route::post('add-card','ApiController@addCard');
Route::post('delete-card','ApiController@deleteCard');
Route::post('cards','ApiController@cards');

Route::post('make-comment','ApiController@makeComment');

Route::post('delete-comment','ApiController@deleteComment');

Route::post('like-post','ApiController@like');

Route::post('unlike-post/','ApiController@unLike');

Route::get('liked-posts','ApiController@likedPosts');



Route::post('posts','ApiController@posts');




//new
Route::post('update-post','ApiController@updatePost');
Route::get('orders-summary/{cid}','ApiController@getOrdersSummary');


Route::get('my-posts/{cid}','ApiController@myPosts');
Route::get('delete-post/{pid}','ApiController@deletePost');


Route::post('make-order/{cid}','ApiController@makeOrder');



Route::post('followers','ApiController@followers');
Route::post('following','ApiController@following');


Route::get('posts-i-like/{cid}','ApiController@postsILike');

//mood
Route::post('add-mood','ApiController@addMood');
Route::get('delete-mood/{moid}','ApiController@deleteMood');
Route::post('add-mood-post','ApiController@addMoodPosts');
Route::get('my-mood/{cid}','Apicontroller@myMood');



//messaging
Route::post('message-users','ApiController@messageUsers');
Route::post('send-message','ApiController@sendMessage');
Route::post('conversation','ApiController@conversation');
Route::post('set-message-read','ApiController@setMessageRead');
Route::post('delete-message','ApiController@deleteMessage');
Route::post('delete-conversation','ApiController@deleteConversation');